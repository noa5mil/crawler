
import cheerio from 'cheerio';
import axios from 'axios';
import fs from 'fs';
import { sub, differenceInMinutes } from 'date-fns';

const URL = 'https://pastebin.com';
const INTERVAL = 2;
const TimeUnit  = {
    MINUTES: 'min',
    SECONDS: 'sec',
}

const getPasteIdsByInterval = async () => {
    const latestPasteId = [];
    const response = await axios.get(`${URL}/archive`);
    const $ = cheerio.load(response.data);
    const now = new Date();

    const tabelElements = $('table.maintable tr');
    tabelElements.each(function () {
        const pasteId = $(this).find('td:first-child>a').attr('href');
        const publishDate = $(this).find('td:nth-child(2)').text().split(' ');

        const minutesFilter = publishDate[1] === TimeUnit.MINUTES && differenceInMinutes(
            now, new Date(sub(now, { 'minutes': publishDate[0] }))) < INTERVAL;
        const secondsFilter = publishDate[1] === TimeUnit.SECONDS;

        if (secondsFilter || minutesFilter) {
            latestPasteId.push(pasteId);
        }
    });
    return latestPasteId;
};

const parsePaste = (paste) => {
    const $ = cheerio.load(paste.data);
    const pasteAttributes = {};
    pasteAttributes['title'] = $('div.info-top>h1').text();
    pasteAttributes['author'] = $('div.info-bottom div.username>a').text();;
    pasteAttributes['date'] = $('div.info-bottom div.date>span').text();
    pasteAttributes['code'] = $('div.highlighted-code div.source').text();
    return pasteAttributes;
}

const writePasteToFile = (paste) => {
    const fileTtitle = `${paste['author']} ${paste['date']}`
    const data = JSON.stringify(paste).replaceAll('\\t', '');
    fs.writeFile(`${fileTtitle}.json`, data, (err) => {
        if (err) throw err;
        console.log(`The file ${fileTtitle}.json has been saved!`);
    });
}

const saveLatestPastes = async () => {
    try {
        const latestPastesIds = await getPasteIdsByInterval();
        const pastes = await Promise.all(
            latestPastesIds.map(
                async pasteId => await axios.get(`${URL}${pasteId}`))
        );
        pastes.map(paste => {
            const pasteData = parsePaste(paste);
            writePasteToFile(pasteData);
        });
    } catch (error) {
        console.log('Error getting latest pastes');
    }
}

setInterval(saveLatestPastes, 120000)
